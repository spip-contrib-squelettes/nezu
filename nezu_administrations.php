<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin nezu
 *
 * @plugin     nezu
 * @copyright  2023
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\nezu\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/meta');
include_spip('inc/cextras');
include_spip('base/nezu');


/**
 * Fonction d'installation et de mise à jour du plugin nezu.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function nezu_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['create']);
	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['1.0.1']);
	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['1.0.2']);
	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['1.0.3']);
	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['1.0.4']);
	cextras_api_upgrade(nezu_declarer_champs_extras(), $maj['1.0.5']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin nezu
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function nezu_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(nezu_declarer_champs_extras());
	effacer_config('nezu');
	effacer_meta($nom_meta_base_version);
}
