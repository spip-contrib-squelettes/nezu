<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function nezu_declarer_champs_extras($champs = array()) {

	// Table spip_rubriques
	$champs['spip_rubriques']['nezu_type_rubrique'] = array(
		'saisie' => 'radio',
		'options' => array(
			'nom' => 'nezu_type_rubrique',
			'label' => _T('nezu:nezu_type_rubrique'),
			'sql' => "varchar(30) NOT NULL DEFAULT ''",
			'defaut' => 'tri_date',
			'data' => array(
				'tri_date' => _T('nezu:nezu_type_rubrique_tri_date'),
 				'tri_num' => _T('nezu:nezu_type_rubrique_tri_num'),
				'tri_faq' => _T('nezu:nezu_type_rubrique_tri_faq'),
				'tri_evenement' => _T('nezu:nezu_type_rubrique_tri_evenement'),
			),
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);


	// Table spip_articles
	$champs['spip_articles']['cacher_a2a'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'cacher_a2a',
			'label_case' => _T('nezu:cacher_a2a'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => '')),	//Tout le monde peuvent modifier
		),
	);
	$champs['spip_articles']['cacher_date'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'cacher_date',
			'label_case' => _T('nezu:cacher_date'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => '')),	//Tout le monde peuvent modifier
		),
	);


	return $champs;
}
